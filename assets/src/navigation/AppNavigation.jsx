import React from 'react'
import {createAppContainer} from 'react-navigation'
// import {NavigationContainer} from '@react-navigation/native'
// import {createStackNavigator} from '@react-navigation/stack'
import {createBottomTabNavigator} from 'react-navigation-tabs'
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs'
import {createDrawerNavigator} from 'react-navigation-drawer'
import {Ionicons, AntDesign, Feather} from '@expo/vector-icons'
import {Platform} from 'react-native'
import { createStackNavigator } from 'react-navigation-stack';

import {MainScreen} from "../screens/MainScreen/MainScreen"
import {PostScreen} from "../screens/PostScreen/PostScreen"
import {BookedScreen} from "../screens/BookedScreen/BookedScreen"
import {AboutScreen} from "../screens/AboutScreen/AboutScreen"
import {HEADER_STYLES, THEME} from "../theme"
import {CreateScreen} from "../screens/CreateScreen/CreateScreen"

const navigatorOptions = {
    defaultNavigationOptions: {
        headerStyle: HEADER_STYLES.headerStyle,
        headerTintColor: HEADER_STYLES.headerTintColor
    }
};

const ScreensNavigation = createStackNavigator(
    {
        Main: MainScreen,
        Post: PostScreen
    },
    navigatorOptions
);

const BookedNavigator = createStackNavigator({
        Booked: BookedScreen
    },
    navigatorOptions
);

// const ScreensNavigation = () => (
//     <NavigationContainer initialRouteName="Main">
//         <Post.Navigator screenOptions={{ headerStyle: HEADER_STYLES.headerStyle, headerTintColor: HEADER_STYLES.headerTintColor }}>
//             <Post.Screen name="Main" component={MainScreen} options={MainScreen.navigationOptions}/>
//
//             <Post.Screen name="Post" component={PostScreen} options={PostScreen.navigationOptions}/>
//         </Post.Navigator>
//     </NavigationContainer>
// );
//
// const BookedNavigator = () => (
//     <NavigationContainer initialRouteName="Booked">
//         <Booked.Navigator>
//             <Booked.Screen name="Booked" component={BookedScreen} options={BookedScreen.navigationOptions}/>
//         </Booked.Navigator>
//     </NavigationContainer>
// );


const bottomTabsConfig = {
    Posts: {
        screen: ScreensNavigation,
        navigationOptions: {
            tabBarLabel: 'All',
            tabBarIcon: info => <Ionicons name="ios-albums" size={25} color={info.tintColor}/>
        }
    },

    Booked: {
        screen: BookedNavigator,
        navigationOptions: {
            tabBarLabel: 'Marked',
            tabBarIcon: info => <Ionicons name="ios-star" size={25} color={info.tintColor}/>
        }
    }
};

const BottomTabNavigator = Platform.OS === 'android'
    ? createMaterialBottomTabNavigator(bottomTabsConfig,
        {
            tabBarOptions: {
                activeTintColor: THEME.DARK_COLOR,
                inactiveTintColor: '#fff',
            },

            shifting: true,
            barStyle: {
                backgroundColor: THEME.MAIN_COLOR
            }
        })

    : createBottomTabNavigator(bottomTabsConfig,
            {
                tabBarOptions: {
                    activeTintColor: THEME.PRIMARY_COLOR,
                    inactiveTintColor: THEME.DARK_COLOR,
                },
            }
        );

const CreateNavigator = createStackNavigator(
    {
        Create: CreateScreen
    },
    navigatorOptions
);

const AboutNavigator = createStackNavigator(
    {
        About: AboutScreen
    },
    navigatorOptions
);

const MainNavigator = createDrawerNavigator(
    {
        Posts: {
            screen: BottomTabNavigator,
            navigationOptions: {
                drawerLabel: 'Home',
                drawerIcon: <AntDesign name="home" size={25}/>
            }
        },
        About: {
            screen: AboutNavigator,
            navigationOptions: {
                drawerLabel: 'About Us',
                drawerIcon: <Feather name="info" size={25}/>
            }
        },
        Create: {
            screen: CreateNavigator,
            navigationOptions: {
                drawerLabel: 'Create Post',
                drawerIcon: <Ionicons name="ios-create" size={25}/>
            }
        },
    },
    {
        contentOptions: {
            activeTintColor: THEME.PRIMARY_COLOR,
            labelStyle: {
                fontFamily: 'open-bold'
            }
        }
    }
);

export const AppNavigation = createAppContainer(MainNavigator);
