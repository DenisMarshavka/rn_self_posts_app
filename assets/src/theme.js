import {Platform} from 'react-native'

export const THEME = {
    MAIN_COLOR: '#FFF41F',
    PRIMARY_COLOR: '#E6DB1C',
    DARK_COLOR: '#403D08',
    DARK_COLOR_HEX: ['64',' 61', '8'],

    PRIMARY_DARKEN_COLOR: '#807A0F',
    MAIN_DARKEN_COLOR: '#C2BA17',

    DANGER_COLOR: 'red',
};

export const HEADER_STYLES = {
    headerStyle: {
        backgroundColor: Platform.OS === 'ios' ? '#fff' : THEME.MAIN_COLOR
    },

    headerTintColor: Platform.OS === 'ios' ? THEME.DARK_COLOR : '#fff'
};
