import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    image: {
        width: '100%',
        height: 300,
    },

    textWrap: {
        width: '100%',
        marginTop: 25,
        paddingHorizontal: 20
    },

    title: {
        fontFamily: 'open-regular'
    },

    btn: {
        marginTop: 30,
    }
});
