import React, {useEffect, useCallback} from 'react'
import { View, Text, Image, Button, ScrollView, Alert } from 'react-native'
import { HeaderButtons, Item } from "react-navigation-header-buttons"
import { useDispatch, useSelector } from "react-redux"

import s from './PostScreen.moduleCSS'
import {DATA} from "../../data"
import {THEME} from "../../theme"
import {AppHeaderIcon} from "../../components/UI/AppHeaderIcon"
import {removePost, toggleBooked} from "../../store/actions/post";

export const PostScreen = ({ navigation }) => {
    const {postId} = navigation.state.params;
    const post     = useSelector(state => state.posts.allPosts.find(post => post.id === postId));
    const dispatch = useDispatch();

    const booked = useSelector(state =>
        state.posts.bookedPosts.some(post => post.id === postId)
    );

    useEffect(() => {
        navigation.setParams({ booked });
    }, [booked]);

    const handleBookedToggle = useCallback(() => {
        dispatch(toggleBooked(post));
    }, [dispatch, post]);

    useEffect(() => {
        navigation.setParams({ handleBookedToggle });
    }, [handleBookedToggle]);

    const handlePostRemove = () => {
        Alert.alert(
            "Delete the post",
            "Are you sure you want to delete the post ?",

            [
                {
                    text: "No",
                    style: "cancel"
                },
                {
                    text: "Yes",
                    style: "destructive",
                    onPress() {
                        navigation.navigate('Main');
                        dispatch(removePost(postId));
                    }
                }
            ],

            { cancelable: false }
        );
    };

    if (!post) return null;

    return (
      <ScrollView>
          <Image style={s.image} source={{uri: post.img}}/>

          <View style={s.textWrap}>
              <Text style={s.title}>{post.text}</Text>
          </View>

          <View style={s.btn}>
              <Button title="Delete" color={THEME.DANGER_COLOR} onPress={handlePostRemove}/>
          </View>
      </ScrollView>
    );
};

PostScreen.navigationOptions = ({ navigation }) => {
    const { booked, date, handleBookedToggle } = navigation.state.params;
    const iconName = booked ? 'ios-star' : 'ios-star-outline';

    return {
        headerTitle: `Post from ${new Date(date).toLocaleDateString()}`,
        headerRight: () => (
            <HeaderButtons title="Bookmarked" HeaderButtonComponent={AppHeaderIcon}>
                <Item title="Toggle bookmarked" iconName={iconName} onPress={handleBookedToggle} />
            </HeaderButtons>
        ),
    }
};
