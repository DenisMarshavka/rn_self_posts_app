import React from 'react'
import {HeaderButtons, HiddenItem, Item} from "react-navigation-header-buttons"
import {useSelector} from 'react-redux'

import s from './BookedScreen.moduleCSS';
import {PostsList} from "../../components/PostsList/PostsList"

import {AppHeaderIcon} from "../../components/UI/AppHeaderIcon"

export const BookedScreen = ({ navigation }) => {
    const handleOpenPost = post => navigation.navigate('Post', { postId: post.id, date: post.date, booked: post.booked });
    const bookedPosts = useSelector(state => state.posts.bookedPosts);

    return <PostsList data={bookedPosts} onOpen={handleOpenPost}/>;
};

BookedScreen.navigationOptions = ({ navigation }) => ({
    title: 'Marked Posts',
    headerLeft: () => (
        <HeaderButtons title="Drawer" HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Toggle Drawer" iconName="ios-menu" onPress={() => navigation.toggleDrawer()} />
        </HeaderButtons>
    ),
    headerRight: () => (
        <HeaderButtons title="Photo" HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Take photo" iconName="ios-camera" onPress={() => navigation.push('Create')} />

            <HiddenItem title="hidden in overflow menu" onPress={() => alert('hidden in overflow')} />
        </HeaderButtons>
    ),
});
