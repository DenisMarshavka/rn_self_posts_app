import React from 'react'
import {View, Text} from 'react-native'
import {HeaderButtons, Item} from "react-navigation-header-buttons"

import {AppHeaderIcon} from "../../components/UI/AppHeaderIcon"

import s from './AboutScreen.moduleCSS'

export const AboutScreen = ({}) => {
  return (
      <View style={s.center}>
          <Text>It's an amazing application for are your Posts</Text>
          <Text>This version Application <Text style={s.version}>1.0.2</Text></Text>
      </View>
  );
};

AboutScreen.navigationOptions = ({ navigation }) => ({
    headerTitle: 'About Us',
    headerLeft: () => (
        <HeaderButtons title="Drawer" HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Toggle Drawer" iconName="ios-menu" onPress={() => navigation.toggleDrawer()} />
        </HeaderButtons>
    )
});
