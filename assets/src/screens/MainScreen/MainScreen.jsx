import React, {useEffect} from 'react'
import { View, ActivityIndicator } from "react-native"
import { HeaderButtons, Item, HiddenItem } from 'react-navigation-header-buttons'
import { useDispatch, useSelector } from "react-redux"

import s from './MainScreen.moduleCSS'
import {THEME} from "../../theme"

import {AppHeaderIcon} from "../../components/UI/AppHeaderIcon"
import {PostsList} from "../../components/PostsList/PostsList"
import {loadPosts} from "../../store/actions/post"

export const MainScreen = ({ navigation }) => {
    const handleOpenPost = post => navigation.navigate('Post', { postId: post.id, date: post.date, booked: post.booked });
    const dispatch = useDispatch();
    const allPosts = useSelector(state => state.posts.allPosts);
    const loading = useSelector(state => state.posts.loading);

    useEffect(() => {
        dispatch(loadPosts());
    }, [dispatch]);

    if (loading) {
        return <View style={s.loading}>
            <ActivityIndicator color={THEME.PRIMARY_DARKEN_COLOR} />
        </View>
    }

    return <PostsList data={allPosts} onOpen={handleOpenPost}/>;
};

MainScreen.navigationOptions = ({ navigation }) => ({
    title: 'My Blog',
    headerLeft: () => (
        <HeaderButtons title="Drawer" HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Toggle Drawer" iconName="ios-menu" onPress={() => navigation.toggleDrawer()} />
        </HeaderButtons>
    ),
    headerRight: () => (
        <HeaderButtons title="Photo" HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Take photo" iconName="ios-camera" onPress={() => navigation.navigate('Create')} />

            <HiddenItem title="hidden in overflow menu" onPress={() => alert('hidden in overflow')} />
        </HeaderButtons>
    ),
});
