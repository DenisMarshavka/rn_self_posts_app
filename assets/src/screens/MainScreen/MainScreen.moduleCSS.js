import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    wrapper: {
        padding: 10,
    },

    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
