import React, { useState } from 'react'
import { View, Text, TextInput, Button, ScrollView } from 'react-native'
import { HeaderButtons, Item } from "react-navigation-header-buttons"
import {useDispatch} from "react-redux"

import s from './CreateScreen.moduleCSS'

import {AppHeaderIcon} from "../../components/UI/AppHeaderIcon"
import {createPost} from "../../store/actions/post"
import {PhotoPicker} from "../../components/UI/PhotoPicker"
import {createPostImg} from "../../store/actions/create"

export const CreateScreen = ({ navigation }) => {
    const dispatch = useDispatch();
    const [text, setText] = useState('');
    const [img, setImg] = useState(null);

    const handleSave = () => {
        const post = {
            date: new Date().toJSON(),
            text,
            img: img,
            booked: false
        };

        dispatch(createPost(post));
        navigation.navigate('Main');

        setText('');
        dispatch(createPostImg(null));
    };

    const handlePick = uri => setImg(uri);

    return (
        <ScrollView>
            <View style={s.wrapper}>
                <Text style={s.title}>Let's create your Post</Text>

                <TextInput
                    style={s.textarea} placeholder="Please enter your text for new Post"
                    value={text} onChangeText={text => setText(text)}
                    multiline
                />
            </View>

            <PhotoPicker onPick={handlePick} />

            <View style={s.wrapper}>
                <Button title="Create" onPress={handleSave} disabled={!text || !img}/>
            </View>
        </ScrollView>
    );
};

CreateScreen.navigationOptions = ({ navigation }) => ({
    headerTitle: 'Create new Post',
    headerLeft: () => (
        <HeaderButtons title="Drawer" HeaderButtonComponent={AppHeaderIcon}>
            <Item title="Toggle Drawer" iconName="ios-menu" onPress={() => navigation.toggleDrawer()} />
        </HeaderButtons>
    )
});

