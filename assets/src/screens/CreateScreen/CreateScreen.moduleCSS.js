import {StyleSheet} from 'react-native'

import {THEME} from "../../theme"

export default StyleSheet.create({
    wrapper: {
        padding: 20,
    },

    title: {
        fontSize: 18,
        marginVertical: 5,

        textAlign: 'center',
        fontFamily: 'open-bold',
    },

    textarea: {
        marginTop: 25,
        paddingTop: 15,
        padding: 15,

        borderWidth: 1,
        borderColor: THEME.DARK_COLOR,
        borderRadius: 20,
    },

    image: {
        width: '100%',
        height: 200,
    }
});
