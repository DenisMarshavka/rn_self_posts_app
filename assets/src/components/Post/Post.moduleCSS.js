import {StyleSheet} from 'react-native'

import {THEME} from "../../theme"

export default StyleSheet.create({
    box: {
        marginBottom: 20,

        overflow: 'hidden',
        borderRadius: 10,
    },

    img: {
        width: '100%',
        height: 200,
    },

    textWrap: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'space-around',
        paddingVertical: 10,
        paddingHorizontal: 25,

        backgroundColor: `rgba(${[...THEME.DARK_COLOR_HEX]}, .6)`,
    },

    title: {
        color: THEME.MAIN_COLOR,
        fontFamily: 'open-bold',
        fontSize: 23,
    },

    date: {
        fontFamily: 'open-regular',
        color: '#fff',
        fontSize: 18,
    }
});
