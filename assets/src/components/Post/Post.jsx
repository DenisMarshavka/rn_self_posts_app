import React from 'react'
import { View, ImageBackground, Text, TouchableOpacity } from 'react-native'

import s from './Post.moduleCSS';

export const Post = ({ post, onOpen }) => {
    return (
        <TouchableOpacity activeOpacity={.6} onPress={() => onOpen(post)}>
            <View style={s.box}>
                <ImageBackground style={s.img} source={{uri: post.img}}>
                    <View style={s.textWrap}>
                        <Text style={s.title}>{post.text.trim().slice(0, 15) + '...'}</Text>

                        <Text style={s.date}>{new Date(post.date).toLocaleDateString()}</Text>
                    </View>
                </ImageBackground>
            </View>
        </TouchableOpacity>
    );
};
