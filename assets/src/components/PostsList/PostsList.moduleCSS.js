import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    wrapper: {
        padding: 10,
    },

    emptyText: {
        marginVertical: 15,

        fontFamily: 'open-regular',
        fontSize: 18,
        textAlign: 'center',
    }
});
