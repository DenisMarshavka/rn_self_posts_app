import React from 'react'
import { View, FlatList, Text } from 'react-native'

import s from './PostsList.moduleCSS'
import {Post} from '../Post/Post'

export const PostsList = ({ data = [], onOpen }) => {
    if (!data.length) {
        return <View style={s.wrapper}>
            <Text style={s.emptyText}>Post lists are empty :(</Text>
        </View>;
    }

    return (
        <View style={s.wrapper}>
            <FlatList
                data={data} keyExtractor={post => post.id.toString()}
                renderItem={({ item }) => <Post post={item} onOpen={onOpen} />}
            />
        </View>
    );
};
