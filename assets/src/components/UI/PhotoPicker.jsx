import React from 'react'
import { View, StyleSheet, Button, Image, Alert } from 'react-native'
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import { useSelector, useDispatch } from "react-redux"

import {createPostImg} from "../../store/actions/create"

const askForPermissions = async () => {
    const {status} = await Permissions.askAsync(
        Permissions.CAMERA,
        Permissions.CAMERA_ROLL
    );

    if (status !== 'granted') {
        Alert.alert("Error", "Not permissions for using a Camera");
        return false;
    }

    return true;
};

export const PhotoPicker = ({ onPick }) => {
    const dispatch = useDispatch();
    const image = useSelector(state => state.create.img);

    const handleTakePhotoPress = async () => {
        const hasPermissions = await askForPermissions();

        if (hasPermissions) {
            const img = await ImagePicker.launchCameraAsync({
                quality: 0.5,
                allowsEditing: false,
                aspect: [16, 9],
            });

            dispatch(createPostImg(img.uri));
            onPick(img.uri);
        }
    };

    return (
        <View style={s.wrapper}>
            <View style={{ ...s.button, paddingBottom: image ? 0 : 10 }}>
                <Button title="Take photo" onPress={handleTakePhotoPress}/>
            </View>

            {image && <Image style={s.image} source={{uri: image}}/>}
        </View>
    );
};

const s = StyleSheet.create({
    wrapper: {
        marginTop: 10,
        marginBottom: 10,
    },

    image: {
        marginTop: 15,

        width: '100%',
        height: 200,
    },

    button: {
        padding: 20,
        paddingTop: 0,
        paddingBottom: 0
    }
});

