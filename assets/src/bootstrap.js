import * as Font from 'expo-font'
import {DB} from './db'

export const bootstrap = async () => {
    try {
        await Font.loadAsync({
            'open-bold': require('../fonts/OpenSans-Bold.ttf'),
            'open-regular': require('../fonts/OpenSans-Regular.ttf')
        });

        await DB.init();

        console.log('Database started...');
    } catch (e) {
        console.log('Error: ', e);
    }
};
