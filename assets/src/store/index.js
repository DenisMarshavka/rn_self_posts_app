import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import {postReducer} from "./reducers/post"
import {createReducer} from "./reducers/create"

const rootReducer = combineReducers({
    posts: postReducer,
    create: createReducer
});

export default createStore(rootReducer, applyMiddleware(thunk));
