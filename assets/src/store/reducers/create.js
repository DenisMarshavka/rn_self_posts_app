import {CHANGE_IMG} from "../types";

const initialState = {
    img: null
};

export const createReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_IMG:
            return {
                ...state,
                img: action.payload,
            };

        default:
            return state;
    }
};
