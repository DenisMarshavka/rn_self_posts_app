import {CHANGE_IMG} from "../types"

export const createPostImg = img => ({
    type: CHANGE_IMG,
    payload: img,
});
