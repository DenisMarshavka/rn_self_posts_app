import {ADD_POST, LOAD_POSTS, REMOVE_POST, TOGGLE_BOOKED} from "../types"
import * as FileSystem from 'expo-file-system'

import {DB} from "../../db"

export const loadPosts = () => async dispatch => {
    const posts = await DB.getPosts();

    dispatch({
        type: LOAD_POSTS,
        payload: posts,
    });
};

export const toggleBooked = post => async dispatch => {
    await DB.upadatePost(post);

    dispatch({
        type: TOGGLE_BOOKED,
        payload: post.id,
    });
};

export const removePost = id => async dispatch => {
    await DB.removePost(id);

    dispatch({
        type: REMOVE_POST,
        payload: id,
    });
};

export const createPost = post => async dispatch => {
    const fileName = post.img.split('/').pop();
    const newPath  = `${FileSystem.documentDirectory}${fileName}`;

    try {
        await FileSystem.moveAsync({
            from: post.img,
            to: newPath,
        });

        post.img = newPath;
        post.id = await DB.createPost(post);

        dispatch({
            type: ADD_POST,
            payload: post
        });
    } catch (e) {
        console.log('Error: ', e);
    }
};
