import React, {useState} from 'react'
import { Text, View } from 'react-native'
import {AppLoading} from 'expo'
import {Provider} from 'react-redux'

import {bootstrap} from "./assets/src/bootstrap"
import {AppNavigation} from "./assets/src/navigation/AppNavigation";
import store from './assets/src/store/index';

export default function App() {
    const [isReady, setIsReady] = useState(false);

    if (!isReady) return (
        <AppLoading
            startAsync={bootstrap} onFinish={() => setIsReady(true)}
            onError={error => console.log('Some error', error)}
        />
    );

    return (
        <Provider store={store}>
            <AppNavigation />
        </Provider>
    );
}
